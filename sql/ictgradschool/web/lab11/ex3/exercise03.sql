-- Answers to Exercise 3 here
DROP TABLE IF EXISTS ex03_videoTable;

CREATE TABLE ex03_videoTable(
  name VARCHAR(40),
  gender CHAR(1),
  year_born INT,
  joined INT,
  num_hires INT,
  PRIMARY KEY (name),
  CHECK (gender like '[FMX]'),
  CHECK (year_born like '[12][0-9][0-9][0-9]'),
  CHECK (joined like '[12][0-9][0-9][0-9]')

);

INSERT INTO ex03_videoTable
VALUES 
           ('Peter Jackson', 'M', '1961', '1997', '17000'),

           ('Jane Campion', 'F', '1954', '1980', '30000'),

           ('Roger Donaldson', 'M', '1945', '1980', '12000'),

           ('Temuera Morrison', 'M', '1960', '1995', '15500'),

           ('Russell Crowe', 'M', '1964', '1990', '10000'),

           ('Lucy Lawless', 'F', '1968', '1995', '5000'),

           ('Michael Hurst', 'M', '1957', '2000', '15000'),

           ('Andrew Niccol', 'M', '1964', '1997', '3500'),

           ('Kiri Te Kanawa', 'F', '1944', '1997', '500'),

           ('Lorde', 'F', '1996', '2010', '1000'),

           ('Scribe', 'M', '1979', '2000', '5000'),

           ('Kimbra', 'F', '1990', '2005', '7000'),

           ('Neil Finn', 'M', '1958', '1985', '6000'),

           ('Anika Moa', 'F', '1980', '2000', '700'),

           ('Bic Runga', 'F', '1976', '1995', '5000'),

           ('Ernest Rutherford', 'M', '1871', '1930', '4200'),

           ('Kate Sheppard', 'F', '1847', '1930', '1000'),

           ('Apirana Turupa Ngata', 'M', '1874', '1920', '3500'),

           ('Edmund Hillary', 'M', '1919', '1955', '10000'),

           ('Katherine Mansfield', 'F', '1888', '1920', '2000'),

           ('Margaret Mahy', 'F', '1936', '1985', '5000'),

           ('John Key', 'M', '1961', '1990', '20000'),

           ('Sonny Bill Williams', 'M', '1985', '1995', '15000'),

           ('Dan Carter', 'M', '1982', '1990', '20000'),

           ('Bernice Mene', 'F', '1975', '1990', '30000');


           