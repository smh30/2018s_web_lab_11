-- Answers to Exercise 9 here
SELECT * FROM ex03_videoTable;

SELECT name, gender, year_born, joined FROM ex03_videoTable;

SELECT title FROM ex04_articles;

SELECT DISTINCT director FROM ex06_moviesTable;

SELECT title from ex06_moviesTable WHERE weeklyPrice <= 2;

SELECT username FROM ex05_table
ORDER BY username;

SELECT username FROM ex05_table
WHERE first_name LIKE 'Pete%';

SELECT username FROM ex05_table
WHERE first_name LIKE 'Pete%' OR last_name LIKE 'Pete%';

