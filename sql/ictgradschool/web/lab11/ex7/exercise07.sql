-- Answers to Exercise 7 here
DROP TABLE IF EXISTS ex07_comments;
CREATE TABLE ex07_comments(
  id INT AUTO_INCREMENT,
  comment_text TEXT,
  article_id INT,
  PRIMARY KEY (id),
  FOREIGN KEY (article_id) REFERENCES ex04_articles(id)
);

INSERT INTO ex07_comments (comment_text, article_id)
VALUES ('fdsjjsifdjo fdshioeji', '12'),
       ('fdijoejfo dfhieoj', 57),
       ('eowqmf fdjie mfeiemjfed!!!!1', 12);

INSERT INTO ex07_comments(comment_text, article_id)
VALUES ('new comment', 57);
