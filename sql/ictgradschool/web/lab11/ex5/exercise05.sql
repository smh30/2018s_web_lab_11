-- Answers to Exercise 5 here
-- Answers to Exercise 2 here
DROP TABLE IF EXISTS ex05_table;
CREATE TABLE ex05_table (
  username   VARCHAR(20),
  first_name VARCHAR(20),
  last_name  VARCHAR(20),
  email      VARCHAR(40),
  PRIMARY KEY (username),
  CHECK (email LIKE '%@%')

);

INSERT INTO ex05_table
VALUES ('programmer1', 'Ada', 'Lovelace', 'love@computers.com'),
       ('spc87', 'Sidney', 'Crosby', 'spc@penguins.com'),
       ('spidey', 'Peter', 'Parker', 'spidey@stark.com'),
       ('wtz', 'Pete', 'Wentz', 'petewtz@decaydance.com'),
       ('human370', 'Sarah', 'Peterson', 'h_u_m@gmail.com'),
       ('e_malkin71Geno', 'Евгений', 'Ма́лкин', 'geno71@penguins.com');

-- INSERT INTO ex05_table
-- VALUES ('spidey', 'Gwen', 'Dunno', 'gwenxx@ht.ik')