-- Answers to Exercise 8 here
DELETE FROM ex06_moviesTable
WHERE id = 8;

ALTER TABLE ex06_moviesTable
    DROP COLUMN weeklyPrice;

DROP TABLE ex06_moviesTable;

DELETE FROM ex06_moviesTable;

UPDATE ex07_comments
SET comment_text = 'a properly written comment'
WHERE id = 2;

UPDATE ex06_moviesTable
SET weeklyPrice = '9'
WHERE director = 'Victor Fleming';

UPDATE ex05_table
SET username = 'peteyw'
WHERE last_name = 'Wentz';

DROP TABLE ex06_moviesTable;
DROP TABLE ex03_videoTable;

