-- Answers to Exercise 4 here
DROP TABLE IF EXISTS ex04_articles;

CREATE TABLE ex04_articles(
  id INT,
  title VARCHAR(100),
  text TEXT,
  PRIMARY KEY (id)
);

INSERT INTO ex04_articles
VALUES ('12', 'Article 01', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),
       ('485', 'Another article', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lectus sit amet est placerat in egestas erat imperdiet. Maecenas pharetra convallis posuere morbi leo urna molestie at. Nisl suscipit adipiscing bibendum est ultricies integer quis auctor elit. Condimentum lacinia quis vel eros donec ac odio. Sit amet purus gravida quis blandit turpis cursus in. Faucibus vitae aliquet nec ullamcorper. Magna fermentum iaculis eu non diam phasellus. Nunc id cursus metus aliquam eleifend mi in nulla posuere. Eget sit amet tellus cras adipiscing enim eu turpis. Sed vulputate mi sit amet mauris commodo quis. Justo donec enim diam vulputate ut pharetra sit amet. Erat imperdiet sed euismod nisi porta lorem mollis aliquam. Bibendum neque egestas congue quisque egestas diam in arcu cursus. Molestie a iaculis at erat pellentesque adipiscing commodo elit at. Nunc consequat interdum varius sit amet mattis vulputate. Vulputate eu scelerisque felis imperdiet proin fermentum leo vel. In ante metus dictum at tempor. Tempor orci dapibus ultrices in iaculis. Fermentum posuere urna nec tincidunt.'),
       ('57', 'The greatest article ever written', 'Auctor augue mauris augue neque gravida in fermentum. Ut morbi tincidunt augue interdum. Aliquam faucibus purus in massa tempor nec feugiat. Ut tortor pretium viverra suspendisse potenti nullam ac tortor. Sagittis id consectetur purus ut faucibus pulvinar elementum integer. Risus viverra adipiscing at in tellus integer feugiat scelerisque. Odio ut enim blandit volutpat maecenas volutpat blandit. Mattis enim ut tellus elementum sagittis vitae. Nibh ipsum consequat nisl vel pretium lectus quam. Diam vel quam elementum pulvinar etiam non. Morbi tristique senectus et netus et malesuada fames ac. Euismod lacinia at quis risus. Nec feugiat in fermentum posuere.');

