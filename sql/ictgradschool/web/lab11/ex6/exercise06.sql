-- Answers to Exercise 6 here
DROP TABLE IF EXISTS ex06_moviesTable;

CREATE TABLE ex06_moviesTable (
  id INT,
  title VARCHAR(40),
  director VARCHAR(40),
  weeklyPrice INT,
  borrower VARCHAR(40) default NULL ,
  PRIMARY KEY (id),
  FOREIGN KEY (borrower) REFERENCES ex03_videoTable (name)
);

INSERT INTO ex06_moviesTable
VALUES ('1', 'The Godfather', 'Francis Ford Coppola', '2', 'Peter Jackson'),
       ('2', 'The Shawshank Redemption', 'Frank Darabont', '4', 'Lorde'),
       ('3', 'Schindler''s List', 'Steven Spielberg', '6', NULL),
       ('4', 'Raging Bull', 'Martin Scorsese', '2', 'Bic Runga'),
       ('5', 'Casablanca', 'Michael Curtiz', '4', 'Margaret Mahy'),
       ('7', 'Gone With The Wind', 'Victor Fleming', '6', NULL ),
       ('8', 'The Wizard of Oz', 'Victor Fleming', '2', NULL),
       ('10', 'Lawrence of Arabia', 'David Lean', '4', 'Anika Moa'),
       ('22', 'E.T. The Extra-Terrestrial', 'Steven Spielberg', '6', NULL)





